org 7C00h
start:
	; init data segment
	mov ax, cs
	mov ds, ax

	call ResetFlpDrv
	
	; init text mode 3
	mov ah, 0
	mov al, 3
	int 10h
   	
   	; show boot message
   	mov ah, 07h
   	mov si, msg_boot
   	call WriteStrAt
   	
   	; read sector into address
	mov ax, 7E0h
	mov es, ax
	xor bx, bx
   	
   	; read sector 2
   	mov ah, 2h
	mov al, 1
	mov ch, 0
	mov cl, 2
	mov dh, 0
	mov dl, 0
	int 13h
	
	jmp kernel

jump:
	jmp jump

; PROCEDURES
	WriteStrAt:
		push bp
		mov bp, sp
		pusha
		mov bx, [textBuffer]
		mov es, bx
		WrtStrStart:
			lodsb
			test al, al
			jz WrtStrFnsh
			stosw
			jmp WrtStrStart
		WrtStrFnsh:
			popa
			mov sp, bp
			pop bp
			ret
		
	ResetFlpDrv:
		mov ah, 0
		mov dl, 0	
		int 13h	
		jc ResetFlpDrv	
		ret	
	
;DATA
	textBuffer dw 0b800h
	msg_kernel db "Kernel Loaded!",0
	msg_boot db "Boot Succesful!",0
	msg_os db "Trial'n'Error: Operating System (C) 2014 by Team Trial'n'Error",0
	
finish:
	db 510 + (start - finish) dup(0)
	db 055h
	db 0AAh
	
org 7E00h
kernel:
	mov ah, 0Ah
   	mov si, msg_kernel
   	mov di, 80 * 2
   	call WriteStrAt
   	
   	mov ah, 0Bh
   	mov si, msg_os
   	mov di, 80 * 4
   	call WriteStrAt
   	
   	mov ah, 2h
   	mov dh, 3
   	mov dl, 0
   	mov bh, 0
   	int 10h
   	
 	cli
	hlt