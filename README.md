# TnE:OS #

**TnE:OS** stands for **T**rial'**n**'**E**rror: **O**perating **S**ystem. It has no real purpose, but to learn Assembly **the hard way.**

![IMG_17062014_021223.png](https://bitbucket.org/repo/kn7b4n/images/3578601729-IMG_17062014_021223.png)

### Why? ###

* Hell if I know.
* To learn Assembly, possibly.
* For fun!
* Was bored...
* We're dumb...


### How to use it? ###

**step1:** You need to make a floppy diskette image, from bin we provide.

**step2:** Otherwise, assemble using FASM our source. jmp step1

### Who made it? ###

* Well, me, RedPanda aka Dmytro Kalchenko
* My friend who's better in asm than me, yuraSniper aka Yuriy Stets
* With possible help of God.